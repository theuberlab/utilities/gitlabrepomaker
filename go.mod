module gitlab.com/theuberlab/utilities/gitlabrepomaker

go 1.15

require (
	github.com/rs/zerolog v1.20.0
	github.com/sirupsen/logrus v1.7.0
)

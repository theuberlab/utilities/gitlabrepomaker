package main

import (
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"text/template"
)

//"github.com/rs/zerolog"
//"github.com/rs/zerolog/log"

var (
	projectDirName		string
	projectLocalPath	string
)

type gitIgnoreMain struct {
	ProjectDirName	string

}

// Applies some common git settings to the repo.
func configGitSettings() error {
	log.Printf("Applying common git settings for %s\n", projectDirName)

	// Set my user name for this repo.
	err := exec.Command("git", "config", "user.name", "Aaron (TheSporkboy) Forster").Run()
	if err != nil {
		return err
	}

	// Set my email for this repo.
	err = exec.Command("git", "config", "user.email", "gitlab@forstersfreehold.com").Run()
	if err != nil {
		return err
	}

	return nil
}

// Fetches a template file.
func fetchTemplateFromURL(url string) (string, error) {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("unable to fetch file from url [%s] error[%s]\n", err)
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error reading response body: Error [%s]\n", err)
	}

	return string(bodyBytes), nil
}

//TODO: Update this to return an error instead of all these log.fatalf() calls.

// Applies some gitignore files to a couple of locations.
func makeGitIgnoreFiles() {
	gitIgnorePath := projectLocalPath + ".gitignore"

	_, err := os.Stat(gitIgnorePath)
	if (err != nil && !os.IsNotExist(err)) {
		log.Printf("Base .gitignore already exists: Overwriting\n")
	}

	// write .gitignore from template here

	templText, err := fetchTemplateFromURL("https://gitlab.com/theuberlab/utilities/gitlabrepomaker/-/raw/master/templates/gitignore-base.tmpl")
	if (err != nil) {
		log.Fatalf("Error fetching gitignore template: Error [%s]\n", err)
	}

	templ, err := template.New(".gitignore").Parse(templText)
	if err != nil {
		log.Fatalf("Error parsing gitignore template: Error [%s]\n", err)
	}

	ignoreTemplVars := gitIgnoreMain{ProjectDirName: projectDirName}

	// Create the file
	outFile, err := os.Create(gitIgnorePath)
	if err != nil {
		log.Fatalf("Error creating .gitignore file: Error [%s]\n", err)
	}
	defer outFile.Close()

	err = templ.Execute(outFile, ignoreTemplVars)
	if err != nil {
		log.Fatalf("Error processing gitignore template: Error [%s]\n", err)
	}


	// Create the .idea/.gitignore file.

	// First check and create .idea
	_, err = os.Stat(projectLocalPath + ".idea")

	if (err != nil && !os.IsNotExist(err)) {
		log.Printf(".idea already exists: skipping creation\n")
	} else {
		err = os.Mkdir(projectLocalPath + ".idea", 0755)
	}

	dotIdeaGitIgnorePath := projectLocalPath + ".idea/.gitignore"
	_, err = os.Stat(dotIdeaGitIgnorePath)

	if (err != nil && !os.IsNotExist(err)) {
		log.Printf(".idea/.gitignore already exists: overwriting\n")
	}

	// Not actually a template currently
	templText, err = fetchTemplateFromURL("https://gitlab.com/theuberlab/utilities/gitlabrepomaker/-/raw/master/templates/gitignore-dotidea.tmpl")
	if (err != nil) {
		log.Fatalf("Error fetching dotidea gitignore file: Error [%s]\n", err)
	}

	// Create the file
	outFile2, err := os.Create(dotIdeaGitIgnorePath)
	if err != nil {
		log.Fatalf("Error creating dotidea gitignore file: Error [%s]\n", err)
	}
	defer outFile2.Close()

	// Write the file
	_, err = outFile2.WriteString(templText)
	if err != nil {
		log.Fatalf("Error writing dotidea gitignore file: Error [%s]\n", err)
	}
	//log.Infof("wrote %d bytes\n", bytesWritten)
}

// Update the gitlab project with the appropriate settings.
// NOT YET IMPLEMENTED!
func updateProjectSettings() error {
	log.Println("updateProjectSettings() not yet implemented. Be sure to update project settings in gitlab.")
	// Set main branch

	// Configure merge settings

	// Configure protected branches

	return nil
}


// Adds the junk I normally initially add.
func initialCommit() error {

	// chreate the 'initial' branch.
	err := exec.Command("git", "checkout", "-b", "initial").Run()
	if err != nil {
		return err
	}

	// Add the gitignore files
	err = exec.Command("git", "add", ".gitignore", ".idea/.gitignore").Run()
	if err != nil {
		return err
	}

	// Commit the gitignore files.
	err = exec.Command("git", "commit", "-m", "'Added gitignore.'").Run()
	if err != nil {
		return err
	}

	return nil
}

// Create some .idea files based on what it would normally create
func addSharedIdeaFiles() error {
	log.Println("addSharedIdeaFiles() not yet implemented. Be sure to add the shared idea files.")

	return nil
}

func main() {
	repoDir, err := exec.Command("git", "rev-parse", "--show-toplevel").Output()
	if err != nil {
		log.Fatal(err)
	}

	projectDirName = filepath.Base(string(repoDir))

	err = configGitSettings()
	if err != nil {
		log.Fatal(err)
	}

	makeGitIgnoreFiles()
	//if err != nil {
	//	log.Fatal(err)
	//}

	updateProjectSettings()

	initialCommit()

	addSharedIdeaFiles()
}

